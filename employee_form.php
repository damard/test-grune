<?php
require_once './theme/header.php';
require_once './config/connection.php';

$query = $pdo_conn->prepare("select * from employee_types  ");
$query->execute();
$jabatan = $query->fetchAll(\PDO::FETCH_ASSOC);
if(isset($_POST['name']))
{
    $name = $_POST['name'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $address = $_POST['address'];
    $sex = $_POST['sex'];

    $employee_type_id = $_POST['employee_type_id'];

    if(isset($_POST['id']))
    {

        $id = $_POST['id'];
        $query = $pdo_conn->prepare("update employees set 
                                name = '$name', 
                                email = '$email', 
                                phone = '$email',
                                address = '$address', 
                                sex = '$sex', 
                                employee_type_id = '$employee_type_id'
               where id = $id
              ");
        $query->execute();
    }
    else{
        $query = $pdo_conn->prepare("insert into employees (name, email, phone,address, sex, employee_type_id) 
              values( '$name', '$email', '$phone', '$address', '$sex', '$employee_type_id' )
              ");
        $query->execute();
    }

    if($query->execute())
    {
        header('Location: employee.php');
    }
}

if(isset($_GET['id'])){
    $id = $_GET['id'];
    $query = $pdo_conn->prepare(" select * from employees where id = $id ");
    $query->execute();
    $result = $query->fetch(\PDO::FETCH_ASSOC);
}
?>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center"><h1>EMPLOYEE ADD / EDIT</h1></div>
            </div>
            <div class="col-md-12">
                <form method="post">
                    <?php
                    if(isset($_GET['id']))
                    {
                        ?>
                        <input type="hidden" name="id" value="<?= $_GET['id'] ?>">
                        <?php
                    }
                    ?>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Name</label>
                        <input type="text" name="name" required class="form-control" value="<?= $result['name']??'' ?>" id="exampleFormControlInput1" placeholder="name">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Email</label>
                        <input type="email" name="email" required class="form-control" value="<?= $result['email']??'' ?>" id="exampleFormControlInput1" placeholder="name@example.com">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Phone</label>
                        <input type="text" name="phone" class="form-control" value="<?= $result['phone']??'' ?>" id="exampleFormControlInput1" placeholder="081XXX">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1" >Address</label>
                        <textarea class="form-control" value="<?= $result['address']??'' ?>" name="address" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" name="sex" type="radio" name="exampleRadios" id="exampleRadios1" value="M" <?= $result['sex']??'' == 'M'? 'checked': ''?> >
                        <label class="form-check-label" for="exampleRadios1">
                            Male
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" name="sex" type="radio" name="exampleRadios" id="exampleRadios2" value="F" <?= $result['sex']??'' == 'F'? 'checked': '' ?> >
                        <label class="form-check-label" for="exampleRadios2">
                           Female
                        </label>
                    </div>

                    <div class="form-group">
                        <label for="exampleFormControlSelect1" >Employee Type</label>
                        <select class="form-control" name="employee_type_id" id="exampleFormControlSelect1">
                            <?php
                            foreach ($jabatan as $nilai)
                            {
                                ?>

                            <option value="<?=$nilai['id']?>"  <?=
                            isset($_GET['id']) ? ($nilai['id']== $result['employee_type_id'] ? 'selected' : '') : ''
                            ?>
                            >
                            <?=$nilai['display_name']?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <button class="btn btn-warning" type="reset">Reset</button>
                    <button class="btn btn-primary" type="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>
<?php
require_once './theme/footer.php';
?>