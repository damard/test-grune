<?php 
require_once './theme/header.php';
require_once './config/connection.php';
$page = 1;
if(!empty($_GET['page'])) {
    $page = $_GET['page'];

}
$items_per_page = 4;
$offset = ($page - 1) * $items_per_page;
$query = $pdo_conn->prepare("select e.*, et.display_name from employees e left join employee_types et on e.employee_type_id = et.id limit $offset , $items_per_page  ");
$query->execute();
$result = $query->fetchAll(\PDO::FETCH_ASSOC);

$total = $pdo_conn->prepare("select count(*) as jumlah from employees e left join employee_types et on e.employee_type_id = et.id");
$total->execute();
$hitung = $total->fetch(\PDO::FETCH_ASSOC);

$pagination = ceil($hitung['jumlah'] / $items_per_page);

?>

<div class="container">
	<div class="row">
        <div class="col-md-12">
            <div class="text-center"><h1>EMPLOYEE LIST</h1></div>
        </div>
        <div class="col-md-12">
            <div class="text-right"><a href="./employee_form.php" class="btn btn-primary">Add</a></div>
            <hr>
        </div>

		<div class="col-md-12">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Sex</th>
                    <th scope="col">Employee Type</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($result as $item)
                {

                ?>
                <tr>
                    <td><?= $item['name'] ?></td>
                    <td><?= $item['email'] ?></td>
                    <td><?= $item['phone'] ?></td>
                    <td><?= $item['sex'] ?></td>
                    <td><?= $item['display_name'] ?></td>
                    <td><a href="./employee_form.php?id=<?= $item['id'] ?>" class='btn btn-primary'>Edit</a>
                        <a href="./delete.php?id=<?= $item['id'] ?>" class='btn btn-danger'>Delete</a></td>
                </tr>
                
                    <?php
                }
                ?>
                </tbody>
            </table>
            <div>
                <div class="float-right">
                <nav aria-label="...">
                    <ul class="pagination pagination-lg">
                        <?php
                        for ($i = 1 ; $i<= $pagination; $i++)
                        {
                            if($pagination == 0)
                            {
                                ?>
                                <li class="page-item">
                                    <a class="page-link" href="./employee.php?page?=1" tabindex="1">1</a>
                                </li>
                                <?php
                            }
                            else{
                            ?>
                            <li class="page-item">
                                <a class="page-link" href="./employee.php?page=<?= $i?>" tabindex="<?= $i?>"><?= $i?></a>
                            </li>
                            <?php
                            }
                        }
                        ?>


                    </ul>
                </nav>
                </div>
            </div>

        </div>
	</div>
</div>
 <?php 
require_once './theme/footer.php';
 ?>